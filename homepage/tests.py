from django.test import TestCase,Client

# Create your tests here.
class TestStory(TestCase):
    def test_halaman_utama(self):
        response = Client().get('/profile/')
        self.assertEqual(200,response.status_code)
    def test_isi_halaman_utama(self):
        response = Client().get('/profile/')
        isi_profile = response.content.decode('utf8')
        self.assertIn("Selamat Datang", isi_profile)
