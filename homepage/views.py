from django.shortcuts import render
from .models import MataKuliah

# Create your views here.
def index(request):
    return render(request,'index.html')

def profile(request):
    return render(request,'profile.html')

def form(request):
    return render(request,'forms.html')

def addMataKuliah(request):
    nama = request.POST['namaMatkul']
    dosen = request.POST['dosenPengajar']
    tahunAjaran = request.POST['tahunAjaran']
    jumlahSKS = request.POST['jumlahSKS']
    ruangKelas = request.POST['ruangKelas']
    deskripsi = request.POST['deskripsi']
    info_matakuliah = MataKuliah(nama=nama,dosen=dosen,tahunAjaran=tahunAjaran,jumlahSKS=jumlahSKS,ruangKelas=ruangKelas,deskripsi=deskripsi)
    info_matakuliah.save()
    return render(request,'forms.html')

def listMatkul(request):
    data = MataKuliah.objects.all()
    matakuliah = {
        "info_matkul" : data
    }
    return render(request,'listMataKuliah.html',matakuliah)

